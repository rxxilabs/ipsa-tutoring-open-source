<?php

	require dirname(dirname(__FILE__)) . '/config/config.php';

	$validateReceived = array(
		'name' => 'Please include your name',
		'email' => 'Please include your email address',
		'phone' => 'Please include a contact number'
	);
	
	foreach ($validateReceived as $key => $value) {
		
		if (!isset($_POST[$key])) {
			echo json_encode(array('error'=> $key.' is missing')); exit;
		}
		
		else if (isset($_POST[$key]) && $_POST[$key] === '') {
			echo json_encode(array('error'=> $value)); exit;
		}
		
	}
	
	/*** Validate Inputs ***/
	
	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
		echo json_encode(array('error'=> 'Please check your email address')); exit;
	}
	
	if(!is_numeric($_POST['phone'])) {
		echo json_encode(array('error'=> 'Please check your number is correct. Remove any spaces')); exit;
	}
	
	/*** Clean Data ***/
	
	function cleanData($str) {
		$str = urldecode ($str );
		$str = filter_var($str, FILTER_SANITIZE_STRING);
		$str = filter_var($str, FILTER_SANITIZE_SPECIAL_CHARS);
		return $str ;
	}
	
	if (isset($_POST['message']) && $_POST['message'] !== '') {
		$_POST['message'] = cleanData($_POST['message']);
	}
	
	/*** Set up PHP Mailer ***/
	
	require '../php/helpers/PHPMailerAutoload.php';
	require '../php/helpers/class.phpMailer.php';
	
	
	$mail = new PHPMailer;
	$mail->isSMTP();                                    	// Set mailer to use SMTP
	$mail->Host = $config->mail->Host;  					// Specify main and backup server
	$mail->SMTPAuth = $config->mail->SMTPAuth;              // Enable SMTP authentication
	$mail->Username = $config->mail->Username;        		// SMTP username
	$mail->Password = $config->mail->Password;         		// SMTP password
	$mail->SMTPSecure = $config->mail->SMTPSecure;          // Enable encryption, 'ssl' also accepted
	
	$mail->From = $config->mail->From;
	$mail->FromName = $config->mail->FromName;
	
	foreach ($config->mail->addAddress as $to) {
		$mail->addAddress($to->email, $to->name);
	}
	
	$mail->addReplyTo($config->mail->addReplyTo->email, $config->mail->addReplyTo->name);
	
	$mail->Subject = 'Hey Remi, Tutoring Services Request';
	
	$mail->isHTML(true);  
	
	/*** Compose Email Message ***/
	$message = 'Hi Remi, you\'ve received a request for your tutoring services: <br/><br/>Here are the details...<br/><br/>';
	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($_POST['name']) . "</td></tr>";
	$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";
	$message .= "<tr><td><strong>Phone:</strong> </td><td>" . strip_tags($_POST['phone']) . "</td></tr>";
	$message .= "<tr><td><strong>Additional Information:</strong> </td><td>" . htmlentities($_POST['message']) . "</td></tr>";
	$message .= "</table>";
	
	$mail->Body    = $message;
	
	$message = "Hi Remi, you\'ve received a request for your tutoring services: \n \n Name: ".$_POST['name']." \n \n Phone: ".$_POST['phone']." \n \n Email:  ".$_POST['email']." \n \n Message: \n ".$_POST['message'];
	
	$mail->AltBody = $message;                                // Set email format to HTML
	
	/*** Send Mail ***/
	
	if(!$mail->send()) {
		echo json_encode(array('error'=> 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo));
		exit;
	}

	echo json_encode(array('success'=> 'Your details have been sent! You will receive a response shortly.'));
	
	exit;
	

?>