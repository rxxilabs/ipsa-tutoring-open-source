<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title>Color Palette by Color Scheme Generator</title>
	<meta name="generator" content="ColorSchemeGenerator.com">

<style type="text/css">

/* Palette color codes */
/* Feel free to copy&paste color codes to your application */

.primary-1 { background-color: #1A9CFF }
.primary-2 { background-color: #4283B4 }
.primary-3 { background-color: #065896 }
.primary-4 { background-color: #56B5FF }
.primary-5 { background-color: #86CAFF }

.secondary-a-1 { background-color: #0AFF89 }
.secondary-a-2 { background-color: #3AB479 }
.secondary-a-3 { background-color: #02964F }
.secondary-a-4 { background-color: #4AFFA8 }
.secondary-a-5 { background-color: #7DFFC1 }

.secondary-b-1 { background-color: #FF490A }
.secondary-b-2 { background-color: #B4593A }
.secondary-b-3 { background-color: #962802 }
.secondary-b-4 { background-color: #FF784A }
.secondary-b-5 { background-color: #FF9E7D }

.complement-1 { background-color: #FF960A }
.complement-2 { background-color: #B4803A }
.complement-3 { background-color: #965702 }
.complement-4 { background-color: #FFB24A }
.complement-5 { background-color: #FFC77D }

/* end */


body {
	margin:0; padding: 2em;
	background:white;
	color: #666;
	font: 75%/1.33 Verdana, sans-serif;
	text-align:left;
	}
h1 {
	margin: 0.5em 0;
	font-size: 200%;
	}
p {
	margin: 0.5em 0;
	}

.color-table {
	margin: 2em 2em 5em;
	border-collapse:collapse;
	border:none;
	border-spacing:0;
	font-size:100%;
	}
.color-table th {
	padding: 0 1em 0 0;
	text-align:right;
	vertical-align: middle;
	font-size:110%;
	border: none;
	}
.color-table td.sample {
	width:10em; height:8em;
	padding: 10px;
	text-align:center;
	vertical-align:middle;
	font-size:90%;
	border: 1px solid white;
	white-space:nowrap;
	}
.color-table.small td.sample {
	width:4em; height:4em;
	padding:0;
	border:none;
	}
.color-table .white { margin-bottom:0.2em; color:white }
.color-table .black { margin-top:0.2em; color:black }

hr {
	border:none;
	border-bottom:1px solid silver;
	}
#footer {
	padding:1em;
	text-align:center;
	font-size:80%;
	}

</style>

</head>
<body>

<h1>Color Palette by Color Scheme Designer</h1>
<p>Palette URL: <a href="http://colorschemedesigner.com/#3x40VuJ--Avxn">http://colorschemedesigner.com/#3x40VuJ--Avxn</a></p>
<p>Color Space: RGB; </p>

<table class="color-table">
	<tr>
		<th>Primary Color:</th>
		<td class="sample primary-1">
			<div class="white">
<strong>1A9CFF</strong>
			</div>
			<div class="black">
<strong>1A9CFF</strong>
			</div>
		</td>
		<td class="sample primary-2">
			<div class="white">
<strong>4283B4</strong>
			</div>
			<div class="black">
<strong>4283B4</strong>
			</div>
		</td>
		<td class="sample primary-3">
			<div class="white">
<strong>065896</strong>
			</div>
			<div class="black">
<strong>065896</strong>
			</div>
		</td>
		<td class="sample primary-4">
			<div class="white">
<strong>56B5FF</strong>
			</div>
			<div class="black">
<strong>56B5FF</strong>
			</div>
		</td>
		<td class="sample primary-5">
			<div class="white">
<strong>86CAFF</strong>
			</div>
			<div class="black">
<strong>86CAFF</strong>
			</div>
		</td>
	</tr>
	<tr>
		<th>Secondary Color A:</th>
		<td class="sample secondary-a-1">
			<div class="white">
<strong>0AFF89</strong>
			</div>
			<div class="black">
<strong>0AFF89</strong>
			</div>
		</td>
		<td class="sample secondary-a-2">
			<div class="white">
<strong>3AB479</strong>
			</div>
			<div class="black">
<strong>3AB479</strong>
			</div>
		</td>
		<td class="sample secondary-a-3">
			<div class="white">
<strong>02964F</strong>
			</div>
			<div class="black">
<strong>02964F</strong>
			</div>
		</td>
		<td class="sample secondary-a-4">
			<div class="white">
<strong>4AFFA8</strong>
			</div>
			<div class="black">
<strong>4AFFA8</strong>
			</div>
		</td>
		<td class="sample secondary-a-5">
			<div class="white">
<strong>7DFFC1</strong>
			</div>
			<div class="black">
<strong>7DFFC1</strong>
			</div>
		</td>
	</tr>
	<tr>
		<th>Secondary Color B:</th>
		<td class="sample secondary-b-1">
			<div class="white">
<strong>FF490A</strong>
			</div>
			<div class="black">
<strong>FF490A</strong>
			</div>
		</td>
		<td class="sample secondary-b-2">
			<div class="white">
<strong>B4593A</strong>
			</div>
			<div class="black">
<strong>B4593A</strong>
			</div>
		</td>
		<td class="sample secondary-b-3">
			<div class="white">
<strong>962802</strong>
			</div>
			<div class="black">
<strong>962802</strong>
			</div>
		</td>
		<td class="sample secondary-b-4">
			<div class="white">
<strong>FF784A</strong>
			</div>
			<div class="black">
<strong>FF784A</strong>
			</div>
		</td>
		<td class="sample secondary-b-5">
			<div class="white">
<strong>FF9E7D</strong>
			</div>
			<div class="black">
<strong>FF9E7D</strong>
			</div>
		</td>
	</tr>
	<tr>
		<th>Complementary Color:</th>
		<td class="sample complement-1">
			<div class="white">
<strong>FF960A</strong>
			</div>
			<div class="black">
<strong>FF960A</strong>
			</div>
		</td>
		<td class="sample complement-2">
			<div class="white">
<strong>B4803A</strong>
			</div>
			<div class="black">
<strong>B4803A</strong>
			</div>
		</td>
		<td class="sample complement-3">
			<div class="white">
<strong>965702</strong>
			</div>
			<div class="black">
<strong>965702</strong>
			</div>
		</td>
		<td class="sample complement-4">
			<div class="white">
<strong>FFB24A</strong>
			</div>
			<div class="black">
<strong>FFB24A</strong>
			</div>
		</td>
		<td class="sample complement-5">
			<div class="white">
<strong>FFC77D</strong>
			</div>
			<div class="black">
<strong>FFC77D</strong>
			</div>
		</td>
	</tr>
</table>

<table class="color-table small">
	<tr>
		<th>Primary Color:</th>
		<td class="sample primary-1"></td>
		<td class="sample primary-2"></td>
		<td class="sample primary-3"></td>
		<td class="sample primary-4"></td>
		<td class="sample primary-5"></td>
	</tr>
	<tr>
		<th>Secondary Color A:</th>
		<td class="sample secondary-a-1"></td>
		<td class="sample secondary-a-2"></td>
		<td class="sample secondary-a-3"></td>
		<td class="sample secondary-a-4"></td>
		<td class="sample secondary-a-5"></td>
	</tr>
	<tr>
		<th>Secondary Color B:</th>
		<td class="sample secondary-b-1"></td>
		<td class="sample secondary-b-2"></td>
		<td class="sample secondary-b-3"></td>
		<td class="sample secondary-b-4"></td>
		<td class="sample secondary-b-5"></td>
	</tr>
	<tr>
		<th>Complementary Color:</th>
		<td class="sample complement-1"></td>
		<td class="sample complement-2"></td>
		<td class="sample complement-3"></td>
		<td class="sample complement-4"></td>
		<td class="sample complement-5"></td>
	</tr>
</table>

<p>
See the HTML source for more details.<br>
Use the <em>Save / Save As...</em> command in your browser to store the HTML for latter use.
</p>

<hr>

<p id="footer">Generated by <a href="http://colorschemedesigner.com">Color Scheme Designer</a> &copy; Petr Stanicek 2002-2010</p>

</body>
</html>
