<?php error_reporting(0); ?>

<!doctype html>
	<html>
		<head>
			<meta charset='utf-8'>
			<meta http-equiv="Cache-Control" content="max-age=2592000"/>
			<meta http-equiv="last-modified" content="2013-08-16@01:00:00 GMT" />
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<meta name="keywords" content="maths, tutor, english, abbey wood, plumstead, charlton, thamesmead, woolwich, tutoring, london, south east">
			
			<title>IPSA Tutoring, Maths &amp; English 11+, Abbey Wood, Plumstead, Charlton, Thamesmead, Woolwich</title>
			
			<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="/css/rocssti.css" />
			<link rel="stylesheet" href="/css/roxxibox.css" />
			<link rel="stylesheet" href="/css/buttons.css" />
			<link rel="stylesheet" href="/css/reveal.css">
			<link rel="stylesheet" href="/css/style.css">
			
			<script type="text/javascript" src="/js/libs/jquery-min.js"></script>
			<script type="text/javascript" src="/js/libs/underscore-min.js"></script>
			<script type="text/javascript" src="/js/libs/imgLiquid-min.js"></script>
			<script type="text/javascript" src="/js/parsley.min.js"></script>
			
			<!--[if lte IE 8]>
				<script type="text/javascript" src="/js/libs/respond.js"></script>
			<![endif]-->
			
			<?php include_once('php/partials/tracking.php'); ?>
		</head>
		<body>
			<div id="wrapper">
				
				<!-- Header -->
				<?php include(__DIR__.'/php/partials/header.php'); ?>
				
				<!-- Headline - eg. Private Tuition For GCSE students -->
				<div class="w100 section" style="background:#ffffff;padding:5em 0 25em 0;">
					<div class="bg">
						<img src="/images/bg/class.jpg" title="" width="500" />
					</div>
					<div class="school-wrapper-width margesauto">
						<div class="row w100">

							<div class="col pr2 pl2 aligncenter">
								
								<h1 class="nomargin headline"><span class="smaller">Prepare your child for success with private tuition</span><br><span class="biggest nocut">Maths, English, 11+ Preparation</span></h1>
								<p><strong>In Abbey Wood, Plumstead, Woolwich, Thamesmead &amp; London South East</strong>
								<p><a data-scroll-nav class="button button-flat-primary p1" href="/#get-in-touch">Get In Touch</a></p>
								
							</div>
						</div>
					</div>
				</div>
				
				<!-- Experience/Qualifcations/Subjects/Levels -->
				<div class="w100 pt1 pb3 section" style="background:#070707;color:#f1f1f1;">
					<div class="school-wrapper-width margesauto pl1 pr1">
						<div class="row w100 ">
							<div class="col">
								<div class="m1">
									<div class="p1">
										<h3 class="nomargin headline">Tailored Tuition</h3>
										<!--<p>I come with 20+ years of experience, having worked in over 10 schools with a variety of students and age groups.</p>-->
										<p>Welcome to our IPSA tutoring, where educational and academic achievement go hand in hand. Our tailored focus on individual child means that every child will find a happy home here.</p>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="m1">
									<div class="p1">
										<h3 class=" nomargin headline">Teaching Levels</h3>
										<!--<p>I teach 11+ Maths, 13+ Maths and GCSE Maths</p>-->
										<p>We specialise in 11+ tutoring, with regular tests under exam condition to achieve maximum sucess. We can also teach Maths and English at KS2 and KS3. Our experienced, skilled teaching staff works to guide your child through his or her academic journey. </p>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="m1">
									<div class="p1">
										<h3 class=" nomargin headline">Qualified &amp; Certified</h3>
										<!--<p>Exam preparation, coursework, help with home work, and building confidence in mathematics</p>-->
										<p>Qualified teaching with proven sucess available locally. CRB certificate is available upon request.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<!-- Testimonials -->
				<div class="w100 pt1 pb3 section" style="background:azure;">
					<div class="mw40em margesauto pl1 pr1">
						<div class="row w100">
							<div class="col">
								<div class="m1">
									<h2 class="aligncenter headline">Testimonials</h2>
								</div>
							</div>
						</div>
						<div class="row w100 ">
							<div class="col50">
								<div class="m1">
									<div class="p1">
										<h3 class="nomargin headline">Sandra Ogunsanya...</h3>
										<p><img src="/images/testimonials/timmy.jpg" style="max-width:40%; float:right;margin:0 0 10px 10px; border:2px solid #070707;" />"I am completely satisfied with the hard work, the assistance and support Remi gave to my son Timmy in preparing him for his Grammar school exams. To top it all, Remi took an interest in him as an individual and have a good knowledge of the child hence tutoring him based on his ability to obtain a high standard of educational achievement. Remi demonstrated an efficient and effective professional approach yet maintaining a friendly environment which helped Timmy assimilate all she taught him. Timmy and I are very grateful for her effort in helping him get into his dream school. I would recommend her to any parent many times more."
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row w100 ">
							<div class="col5 clear">
								<div class="m1">
									<div class="p1">
										<h3 class="nomargin headline">Hannah...</h3>
										<p><img src="/images/testimonials/dammy.jpg" style="max-width:40%; float:right;margin:0 0 10px 10px; border:2px solid #070707;" />Dammy started studying for his 11+ exams in January 2012. He had a very brilliant one to one tutoring with Remi. He did his exams and he passed and are now in Beths Grammar School. I would recommend her for all parents who are looking for similar help for their children because she is patient, firm but fair with them."
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				<!-- Location -->
				
				<!-- Pricing -->
				<div class="w100 pt1 section" style="padding-bottom:30em;">
					<div class="bg">
						<img src="/images/bg/fun-study.jpg" title="" width="500" />
					</div>
					<div class="school-wrapper-width margesauto pl1 pr1">
						<div class="row w100">
							<div class="col">
								<div class="m1">
									<h2 class="aligncenter headline">Pricing</h2>
								</div>
							</div>
						</div>
						<div class="row w100 ">
							<div class="col">
								<div class="m1 aligncenter">
									<div class="p1">
										<h3 class=" nomargin">One to one</h3>
										<p>£20/hr</p>
										<p><a data-scroll-nav class="button button-flat-highlight p1" href="/#get-in-touch">Book</a></p>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="m1 aligncenter">
									<div class="p1">
										<h3 class=" nomargin">Group Class 4-6</h3>
										<p>£25/2hrs</p>
										<p><a data-scroll-nav class="button button-flat-highlight p1" href="/#get-in-touch">Book</a></p>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="m1 aligncenter">
									<div class="p1">
										<h3 class=" nomargin">Discount</h3>
										<p>£36 for 2 hours or £180 for 10 hours</p>
										<p><a data-scroll-nav class="button button-flat-highlight p1" href="/#get-in-touch">Book</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Book A Lesson + Form -->
				<div id="get-in-touch" class="w100 pb1 section school-bg-darkblue white">
					<div class="school-wrapper-width margesauto pl1 pr1">
						<div class="row w100">
						
							<div class="col">
								<div class="m1">
									<p><span class="alignmiddle inlineblock mr1"><img src="/images/icons/tablet.png" /></span><span class="inlineblock">Mob. 07806 097 780</span></p>
									<p><span class="alignmiddle inlineblock mr1"><img src="/images/icons/phone.png" /></span><span class="inlineblock">Tel. 0208 310 9829</span></p>
								</div>
							</div>
							<div class="col">
								<div class="m1">
									<p><span class="aligntop inlineblock mr1 pb0-5 pt0-5"><img src="/images/icons/mail.png" /></span><span class="inlineblock">127 Eynsham Drive <br>Abbey Wood, <br>London, <br>Greater London, <br>SE2 9PU</span></p>
								</div>
							</div>
							<div class="col">
								<div class="m1">
									<h2 class="uppercase headline">Get In Touch</h2>
									<p>Fill in this form, with your details and we will contact you very soon</p>
									<form class="bookingForm" id="booking" data-validate="parsley" name="booking" accept-charset="utf-8">
										<fieldset>
											<label><span>Name*</span></label><input class="name" name="name" type="text" data-required="true" />
										</fieldset>
										<fieldset>
											<label><span>Email*</span></label><input class="email" name="email" type="text" data-trigger="change" data-required="true" data-type="email" />
										</fieldset>
										
										<fieldset>
											<label><span>Contact Number*</span></label><input class="phone" name="text" type="text" data-type="phone" data-required="true" />
										</fieldset>
										<fieldset>
											<label><span>Additional Information? (optional)</span></label><textarea class="message" name="message"></textarea>
										</fieldset>
										<fieldset>
											<input name="submit" class="button-flat-primary" type="submit" value="Send" />
										</fieldset>
										<div class="loader" style="display:none;"><img src="/images/graphics/loader.gif" width="" /></div>
										<div class="school-bg-green w100 response p0-5 stopPadding" style="display:none;"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Footer -->
				<?php include(__DIR__.'/php/partials/footer.php'); ?>
				
				
			</div>
			
			<script>
			
				;(function($) {
					
					/**
					 *@name Image Liquid
					 */
					if ($('.bg').length > 0) {
						$(".bg").imgLiquid();
					}
					
					var $el = $( '#booking' )
					
					$el.parsley( 'addListener', {
						onFormSubmit: function ( formIsValid, event ) {
						
							if (formIsValid) {
								
								event.preventDefault()
								
								$el.find('.loader').fadeIn()
								
								$.ajax({
									url: '/ajax/mail.php',
									type: 'post',
									data: {
										name: $el.find('.name').val(),
										email: $el.find('.email').val(),
										phone: $el.find('.phone').val(),
										message: $el.find('.message').val()
									},
									dataType: 'json',
									success: function(data) {
										$el.find('.response ').html(data.success).fadeIn()
									},
									error: function() {
									
									}
								}).done(function() {
									$el.find('input[type=text], textarea').val('')
									$el.find('.loader').slideUp()
								})
								
							}
						}
					} );
					
					
					
				})(jQuery)
				
				/**
				 *@name Scroll Navigate
				 */
				
				var RoXX = RoXX || {}
				
					RoXX.scrollNavigate = (function($) {
				
						var Init = function(el) {
							
							$(el).each(function() {
							
								$(this).click(function(e) {
									e.preventDefault()
									
									var id = $(this).attr('href').split('#')
									
									
									$('body, html').animate({
										scrollTop: $('#'+id[1]).offset().top
									}, 1000, "swing")
								})
							
							})
							
						}
						
						return Init
				
					})(jQuery)
					
				/**
				 * ACTIONS
				 */
				;(function($) {
					RoXX.scrollNavigate('[data-scroll-nav]')
				})(jQuery)
			
			</script>
			
			
			
			
		</body>
	</html>