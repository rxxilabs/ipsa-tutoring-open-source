
<div class="w100" style="background:black;color:white;">
	<div class="school-wrapper-width margesauto">
		<div class="ml1 mr1">
			<div class="nocut alignmiddle inlineblock mr2 uppercase"><strong>IPSA Tutoring UK &copy; <?php echo date('Y'); ?></strong></div>
			<div class="nocut alignmiddle inlineblock mr2 uppercase"><a href="/terms" class="white nounderline">Terms</a></div>
			<div class="nocut alignmiddle inlineblock mr2 uppercase"><a href="/privacy" class="white nounderline">Privacy</a></div>
		</div>
	</div>
</div>